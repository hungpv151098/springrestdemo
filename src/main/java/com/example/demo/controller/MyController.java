package com.example.demo.controller;

import com.example.demo.pojo.Book;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {

    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer one,
                @RequestParam("b") Integer two) {
        return one + two;
    }

    @GetMapping("/factorial")
    Long factorial(@RequestParam("a") Integer n) {
        if(n == 0){
            return 0L;
        }
        if (n >= 1) {
            return n * factorial(n - 1);
        } else {
            return 1L;
        }
    }

    @PostMapping("/save-book")
    String saveBook(@RequestBody Book book) {
        return book.getName();
    }
}
